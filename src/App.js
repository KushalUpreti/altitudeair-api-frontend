import { useEffect, useState } from 'react';
import './App.css';
import Form from './Components/Form/Form';
import BlogList from './Components/BlogList/BlogList';

function App() {
  const initialObj = {
    title: "",
    description: "",
    content: "",
    date: "",
    publisher: "",
    isFeatured: false
  }

  const [blogs, setBlogs] = useState([]);


  const [mode, setMode] = useState("Add");
  const [selected, setSelected] = useState(null);
  const [formData, setFormData] = useState(initialObj)

  useEffect(() => {
    fetchAllBlogs();
  }, [])

  async function fetchAllBlogs() {
    let response = await fetch('http://altitudeairbeta.kurmatechnepal.com/blog/', {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json'
      },
    });
    let res = await response.json();
    setBlogs(res.data)
  }

  const resetState = () => {
    setFormData(initialObj);
    setMode("Add");
    setSelected(null);
  }

  const addHandler = async () => {
    if (mode === "Edit") {
      let response = await fetch('http://altitudeairbeta.kurmatechnepal.com/blog/' + selected + '/', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(formData)
      });

      if (!response.status === 201) { return }

      let index = blogs.findIndex(item => {
        return item.id === selected;
      })
      const newArray = [...blogs];
      newArray[index] = formData;
      setBlogs(newArray);
      resetState();
      return;
    }

    let response = await fetch('http://altitudeairbeta.kurmatechnepal.com/blog/', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(formData)
    });
    if (!response.status === 201) { return }
    let res = await response.json();
    let array = [...blogs];
    array.unshift(res.data);
    setBlogs(array);

  }

  const editHandler = (id) => {
    setMode("Edit");
    setSelected(id);
    let index = blogs.findIndex(item => {
      return item.id === id;
    })
    setFormData(blogs[index])
  }

  const deleteHandler = async (id) => {

    let response = await fetch('http://altitudeairbeta.kurmatechnepal.com/blog/' + id + "/", {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json'
      },
      body: formData
    });

    if (!response.status === 201) { return }
    let index = blogs.findIndex(item => {
      return item.id === id;
    })
    const newArray = [...blogs];
    newArray.splice(index, 1);
    setBlogs(newArray);
    resetState();
  }

  return (
    <main className="App">
      <section>
        <Form formData={formData} setFormData={setFormData} addHandler={addHandler} mode={mode} />
      </section>
      <section>
        <BlogList blogs={blogs}
          editHandler={editHandler}
          deleteHandler={deleteHandler}
        />
      </section>
    </main>
  );
}

export default App;
