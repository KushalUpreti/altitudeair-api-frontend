import classes from './Form.module.css';

export default function Form({ formData, setFormData, addHandler, mode }) {
    return <form className={classes.form} onSubmit={(e) => { e.preventDefault() }}>
        <input type="text" placeholder="Title" value={formData.title} onChange={(e) => {
            setFormData({ ...formData, title: e.target.value })
        }} />
        <input type="text" placeholder="Description" value={formData.description} onChange={(e) => {
            setFormData({ ...formData, description: e.target.value })
        }} />
        <textarea name="" id="" cols="30" rows="10" placeholder="Content" onChange={(e) => {
            setFormData({ ...formData, content: e.target.value })
        }} value={formData.content} />
        <input type="datetime-local" value={formData.date} onChange={(e) => {
            setFormData({ ...formData, date: e.target.value })
        }} />
        <input type="text" placeholder="Publisher" value={formData.publisher} onChange={(e) => {
            setFormData({ ...formData, publisher: e.target.value })
        }} />

        <div className={classes.select_wrapper}>
            <label htmlFor="select">Is Featured?</label>
            <input type="checkbox" name="" id="" checked={formData.isFeatured} onChange={(e) => {
                setFormData({ ...formData, isFeatured: !formData.isFeatured })
            }} />
        </div>
        <button onClick={addHandler}>{mode}</button>
    </form>
}