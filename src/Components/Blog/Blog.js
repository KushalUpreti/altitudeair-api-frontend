import classes from "./Blog.module.css";
export default function Blog({ id, publisher, date, title, description, content, isFeatured, editHandler, deleteHandler }) {
    return <article className={classes.blog}>
        <div className={classes.date_author_flex}>
            <h3 className={classes.author}>{publisher}</h3>
            <p className={classes.date}>{date}</p>
        </div>

        <h1 className={classes.title}>{title}</h1>
        <p className={classes.description}>{description}</p>
        <br />
        <p className={classes.content}>{content}</p>

        <h4>{isFeatured ? "Featured" : "Not Featured"}</h4>

        <button className={classes.edit} onClick={() => { editHandler(id) }}>Edit</button>
        &ensp;
        <button className={classes.delete} onClick={() => { deleteHandler(id) }}>Delete</button>
    </article>
}