import Blog from "../Blog/Blog";
import classes from "./BlogList.module.css";

export default function BlogList({ blogs, editHandler, deleteHandler }) {

    return <div className={classes.blogList}>
        {blogs.map(item => {
            return <Blog {...item} key={Math.random() * 999} editHandler={editHandler} deleteHandler={deleteHandler} />
        })}
    </div>
}